/*
 *  femto.c: Femto application for MUL Controller 
 *  Copyright (C) 2013, KulCloud Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "config.h"
#include "mul_common.h"
#include "mul_vty.h"
#include "femto.h"

extern struct mul_app_client_cb femto_app_cbs;

    static void 
femto_add(mul_switch_t *sw)
{
    c_log_debug("Femto Switch 0x%llx added", (unsigned long long)(sw->dpid));
}

    static void
femto_del(mul_switch_t *sw)
{
    c_log_debug("Femto Switch 0x%llx removed", (unsigned long long)(sw->dpid));
}

#if 0 /* This is for communication between apps */
mul_service_t *femto_service; /* Femto Service Instance */
static void
mul_femto_service_conn_event(void *service UNUSED, unsigned char conn_event)
{
    c_log_err("%s: %d", FN, conn_event);
}
#endif

/* Vendor message - This is only for TEST */
struct c_ofp_rf_dp {
    uint32_t                   bearer_id;
    uint32_t                   user_id;
    uint64_t                   rf_value;
};

    static void
femto_vendor_msg(mul_switch_t *sw, uint8_t *msg, size_t pkt_len)
{
    //struct ofp_vendor_specific_msg	*ofpv_msg = (void *)msg;
    struct vendor_specific_header	*vendor_hdr;

    struct c_ofp_rf_dp *dp = (void *)msg;
/* This is only for TEST */
    c_log_debug("bearer_id:%x",dp->bearer_id);
    c_log_debug("user_id:%x",dp->user_id);
    c_log_debug("rf_value:%x",dp->rf_value);
    //c_log_debug("FEMTO GET MSG !! pkt_len:%d",pkt_len);




#if 0 
    if(!ofpv_msg){
	c_log_err("%s: Invaild arg", FN);
	return;
    }

    vendor_hdr = (void *)(ofpv_msg);
    
    switch(ofpv_msg->header.type){
	case OFPVSMT_FEMTO_CONFIGURATION_MESSAGE:
	    {
		struct ofp_vendor_femto_config_msg	*ofpv_femto_cfg_msg;
		ofpv_femto_cfg_msg = (void *)(vendor_hdr);

		c_log_debug("%s: OFPVSMT_FEMTO_CONFIGURATION_MESSAGE", FN);

		break;
	    }

	case OFPVSMT_FEMTO_STATUS_REPORT:
	    {
		struct ofp_vendor_status_report		*ofpv_status_rpt = (void *)(vendor_hdr);

		c_log_debug("%s: OFPVSMT_FEMTO_STATUS_REPORT", FN);
		switch(ntohl(ofpv_status_rpt->event)){
		    case VSSRE_TERMINAL_ENTRANCE:
			{
			    struct event_terminal_entrance_report *event_enter_rpt;
			    event_enter_rpt = (void *)(ofpv_status_rpt->body);

			    c_log_err("%s: 0x%x vendor terminal entrance", 
				    FN, ntohl(event_enter_rpt->imsi));
			    c_log_err("bearer status: 0x%x, terminal ip: 0x%x", 
				    ntohl(event_enter_rpt->bearer_status),
				    ntohl(event_enter_rpt->terminal_ip));
			}
			break;

		    case VSSRE_TERMINAL_LEAVE:
			{
			    struct event_terminal_leave_report *event_leave_rpt;
			    event_leave_rpt = (void *)(ofpv_status_rpt->body);

			    c_log_debug("%s: VSSRE_TERMINAL_LEAVE", FN);
			    c_log_err("%s: 0x%x vendor terminal leave", 
				    FN, ntohl(event_leave_rpt->imsi));
			}
			break;

		    case VSSRE_BEARER_STATUS_REPORT:
			{
			    struct event_bearer_status_report_update *event_bearer_rpt;
			    event_bearer_rpt = (void *)(ofpv_status_rpt->body);

			    c_log_debug("%s: VSSRE_BEARER_STATUS_REPORT", FN);
			    c_log_err("%s: 0x%x vendor terminal leave", 
				    FN, ntohl(event_bearer_rpt->imsi));
			    c_log_err("bearer id: %x", ntohl(event_bearer_rpt->bearer_id));
			}
			break;

		    case VSSRE_CHANNEL_STATUS_REPORT:
			{
			    struct event_channel_status_report_update *event_channel_rpt;
			    event_channel_rpt = (void *)(ofpv_status_rpt->body);
			    c_log_debug("%s: VSSRE_CHANNEL_STATUS_REPORT", FN);

			    switch(event_channel_rpt->channel_status){
				case CHANNEL_STATUS_OVERLOADED:
				    break;
				case CHANNEL_STATUS_NORMAL:
				    break;
				case CHANNEL_STATUS_PERIODIC:
				    break;
			    }
			}
			break;

		}
		break;
	    }

	case OFPVSMT_WIFI_CONFIGURATION_MESSAGE:
	    break;

	case OFPVSMT_WIFI_STATUS_REPORT:
	    break;

	case OFPVSMT_SDN_SWITCH_CONFIGURATION_MESSAGE:
	    break;

	default:
	    break;
    }	
#endif
}


#if 0 /* This is for communication between apps */
int
mul_femto_host_mod(void *service, uint8_t test)
{
#if 0
    struct cbuf *b;
    struct c_femto_app_config_test *cf_auc;
    //struct c_ofp_auxapp_cmd *cofp_auc;
    //struct c_ofp_host_mod *cofp_hm;
    int ret = -1;

    if (!service) return ret;
    b = of_prep_msg(sizeof(struct c_femto_app_config), C_OFPT_AUX_CMD, 0);

    cf_auc = (void *)(b->data);
    cf_auc->s_stat = 11;
    cf_auc->l_stat = 22;
    cf_auc->s_dst_prefixlen = 33;
    cf_auc->l_dst_prefixlen = 44;
   
    c_service_send(service, b);

    b = c_service_wait_response(service);
    if (b) {
        if (check_reply_type(b, C_AUX_CMD_SUCCESS)) {
            ret = 0;
        }
        free_cbuf(b);
    }
    
    b = of_prep_msg(sizeof(struct c_ofp_auxapp_cmd) +
                    sizeof(struct c_ofp_host_mod),
                    C_OFPT_AUX_CMD, 0);

    cofp_auc = (void *)(b->data);
    cofp_auc->cmd_code = add ? htonl(C_AUX_CMD_FAB_HOST_ADD): 
                               htonl(C_AUX_CMD_FAB_HOST_DEL);
    cofp_hm = (void *)(cofp_auc->data);
    cofp_hm->switch_id.datapath_id = htonll(dpid);
    memcpy(&cofp_hm->host_flow, fl, sizeof(*fl));
    
    c_service_send(service, b);
    b = c_service_wait_response(service);
    if (b) {
        if (check_reply_type(b, C_AUX_CMD_SUCCESS)) {
            ret = 0;
        }
        
        free_cbuf(b);
    }
    return ret;
#endif
}
#endif

enum femto_app_send_msg_type {
    FEMTO_CONF_LIPA= 1, 
    FEMTO_CONF_SIPTO
};

struct femto_info_header {
	uint16_t 	type;  	/*One of the OFPVSMT_* Constants */
	uint16_t	length;
	uint8_t		pad[4];
};

struct ofp_app_send_femto_info {
	struct femto_info_header 	header;
	uint8_t 		        body[0];	/* Body of the request message */
};

struct femto_app_conf_lipa
{
    uint32_t l_dst_ip;
    uint16_t length;
    uint8_t  index;
    uint8_t  l_stat;
    uint8_t  l_dst_prefixlen;
    uint8_t  pad[7];
};

struct femto_app_conf_sipto
{
    uint32_t s_gw_ip;
    uint32_t s_dst_ip;
    uint16_t length;
    uint8_t  index;
    uint8_t  s_stat;
    uint8_t  s_gw_prefixlen;
    uint8_t  s_dst_prefixlen;
    uint8_t  pad[2];
};

#define FEMTO_CONF_CMD_ADD     1
#define FEMTO_CONF_CMD_DEL     2 

#define FEMTO_CONF_DISABLE     0 
#define FEMTO_CONF_ENABLE      1 

struct global_femto_app_config
{
    uint8_t  l_stat[5];
    uint32_t l_dst_ip[5];
    uint8_t  l_dst_prefixlen[5];

    uint8_t  s_stat[5];
    uint32_t s_gw_ip[5];
    uint8_t  s_gw_prefixlen[5];
    uint32_t s_dst_ip[5];
    uint8_t  s_dst_prefixlen[5];
};

struct global_femto_app_config g_fem_conf;

static int
update_global_femto_lipa(int cmd, uint8_t index, uint32_t dst_ip, uint8_t prefixlen)
{
    if ( FEMTO_CONF_CMD_ADD == cmd ){
	g_fem_conf.l_stat[index-1]          = FEMTO_CONF_ENABLE;
	g_fem_conf.l_dst_ip[index-1]        = dst_ip;
	g_fem_conf.l_dst_prefixlen[index-1] = prefixlen;
    }
    else if ( FEMTO_CONF_CMD_DEL == cmd ){
	g_fem_conf.l_stat[index-1]          = FEMTO_CONF_DISABLE;
	g_fem_conf.l_dst_ip[index-1]        = 0;
	g_fem_conf.l_dst_prefixlen[index-1] = 0;
    }
    else
	return -1;

    return 0;
}

static int
update_global_femto_sipto(int cmd, uint8_t index, uint32_t dst_ip, uint8_t dst_prefixlen,
	uint32_t gw_ip, uint8_t gw_prefixlen)
{

    if ( FEMTO_CONF_CMD_ADD == cmd ){
	g_fem_conf.s_stat[index-1]            = FEMTO_CONF_ENABLE;
	g_fem_conf.s_dst_ip[index-1]          = dst_ip;
	g_fem_conf.s_dst_prefixlen[index-1]   = dst_prefixlen;
	g_fem_conf.s_gw_ip[index-1]           = gw_ip;
	g_fem_conf.s_gw_prefixlen[index-1]    = gw_prefixlen;
    }
    else if ( FEMTO_CONF_CMD_DEL == cmd ){
	g_fem_conf.s_stat[index-1]            = FEMTO_CONF_DISABLE;
	g_fem_conf.s_dst_ip[index-1]          = 0;
	g_fem_conf.s_dst_prefixlen[index-1]   = 0;
	g_fem_conf.s_gw_ip[index-1]           = 0;
	g_fem_conf.s_gw_prefixlen[index-1]    = 0;
    }
    else
	return -1;

    return 0;
}

DEFUN (femto_lipa_config,
	femto_lipa_config_cmd,
	"femto switch X lipa <1-5> dst-ip A.B.C.D/M",
	"femto configuration\n"
	"openflow-switch\n"
	"datapath-id in 0xXXX format\n"
	"LIPA\n"
	"LIPA target index\n"
	"destination IP\n"
	"IP address\n")
{
    struct prefix_ipv4           dst_ip;
    int ret, num;
    uint16_t fi_len, fl_len;
    void *ptr1 = NULL, *ptr2 = NULL;
    struct ofp_app_send_femto_info  *fi;
    struct femto_app_conf_lipa   *fem_lipa;

    //uint64_t dpid = 0x1000e4115b749f00; /* temp */
    uint64_t dpid; 
    uint32_t vendor_id = 0x0000a;

    dpid = strtoull(argv[0], NULL, 16);
    num = atoi(argv[1]);

    ret = str2prefix(argv[2], (void *)&dst_ip);
    if (ret <= 0) {
        vty_out (vty, "%% Malformed address%s", VTY_NEWLINE);
	return CMD_WARNING;
    }

    ret = update_global_femto_lipa(FEMTO_CONF_CMD_ADD, num, ntohl(dst_ip.prefix.s_addr), dst_ip.prefixlen);

    fi_len = sizeof(struct ofp_app_send_femto_info);
    fl_len = sizeof(struct femto_app_conf_lipa);

    fi = calloc(1, sizeof(fi_len+fl_len));
    fi->header.type = FEMTO_CONF_LIPA;
    fi->header.length = fi_len + fl_len;

    ptr1 = ASSIGN_PTR(fi->body);
    fem_lipa = ptr1;

    fem_lipa->l_stat = FEMTO_CONF_ENABLE;
    fem_lipa->length = fl_len;
    fem_lipa->index = num;
    fem_lipa->l_dst_ip = ntohl(dst_ip.prefix.s_addr);
    fem_lipa->l_dst_prefixlen = dst_ip.prefixlen;

    mul_app_send_vendor_msg(dpid, vendor_id, fi, fi_len+fl_len);
    free(fi); 

#if 0 /* This is for communication between apps */
    femto_service = mul_app_get_service_notify(MUL_FEMTO_SERVICE_NAME,
	    mul_femto_service_conn_event,
	    false, NULL);
    mul_femto_host_mod(femto_service, 0);
#endif

    return CMD_SUCCESS;
}

DEFUN(femto_sipto_config_add,
	femto_sipto_config_cmd,
	"femto switch X sipto <1-5> dst-ip A.B.C.D/M gateway-ip A.B.C.D/M",
	"femto configuration\n"
	"openflow-switch\n"
	"datapath-id in 0xXXX format\n"
	"SIPTO\n"
	"SIPTO target index\n"
	"destination IP\n"
	"IP address\n"
	"gateway IP\n"
	"IP address\n")
{
    struct prefix_ipv4           dst_ip, gw_ip;
    int ret, num;
    uint16_t fi_len, fs_len;
    void *ptr1 = NULL, *ptr2 = NULL;
    struct ofp_app_send_femto_info  *fi;
    struct femto_app_conf_sipto  * fem_sipto;
   
    /* temp dpid, vendor_id*/
    //uint64_t dpid = 0x1000e4115b749f00; 
    uint64_t dpid; 
    uint32_t vendor_id = 0x0000a;

    dpid = strtoull(argv[0], NULL, 16);
    num = atoi(argv[1]);

    ret = str2prefix(argv[2], (void *)&dst_ip);
    if (ret <= 0) {
	vty_out (vty, "%% Malformed dst IP address%s", VTY_NEWLINE);
	return CMD_WARNING;
    }

    /* get string gateway IP */
    ret = str2prefix(argv[3], (void *)&gw_ip);
    if (ret <= 0) {
	vty_out (vty, "%% Malformed gateway IP address%s", VTY_NEWLINE);
	return CMD_WARNING;
    }

    ret = update_global_femto_sipto(FEMTO_CONF_CMD_ADD, num, ntohl(dst_ip.prefix.s_addr), dst_ip.prefixlen, ntohl(gw_ip.prefix.s_addr), gw_ip.prefixlen);

    fi_len = sizeof(struct ofp_app_send_femto_info);
    fs_len = sizeof(struct femto_app_conf_sipto);

    fi = calloc(1, sizeof(fi_len+fs_len));
    fi->header.type = FEMTO_CONF_SIPTO;
    fi->header.length = fi_len + fs_len;

    ptr1 = ASSIGN_PTR(fi->body);
    fem_sipto = ptr1;

    fem_sipto->index             = num;
    fem_sipto->s_stat            = FEMTO_CONF_ENABLE;
    fem_sipto->s_dst_ip          = ntohl(dst_ip.prefix.s_addr);
    fem_sipto->s_dst_prefixlen   = dst_ip.prefixlen;
    fem_sipto->s_gw_ip           = ntohl(gw_ip.prefix.s_addr);
    fem_sipto->s_gw_prefixlen    = gw_ip.prefixlen;

    mul_app_send_vendor_msg(dpid, vendor_id, fi, fi_len+fs_len);
    free(fi); 

#if 0 /* This is for communication between apps */
    femto_service = mul_app_get_service_notify(MUL_FEMTO_SERVICE_NAME,
	    mul_femto_service_conn_event,
	    false, NULL);
    mul_femto_host_mod(femto_service, 0);
#endif

    return CMD_SUCCESS;

}


DEFUN (show_femto_config,
       show_femto_config_cmd,
       "show femto configuration",
       SHOW_STR
       "femto configuration\n"
       "femto LIPA/SIPTO target information\n")
{

    int i=0;



    vty_out(vty, "============================================%s" , VTY_NEWLINE);
    vty_out(vty, "===== FEMTO Target Network Information =====%s" , VTY_NEWLINE);
    vty_out(vty, "============================================%s" , VTY_NEWLINE);
    for(i=0;i<5;i++){

	vty_out(vty, "## Target Index < %d >%s" , i+1,VTY_NEWLINE);
	vty_out(vty, " lipa_status     :%d  %s", g_fem_conf.l_stat[i]     , VTY_NEWLINE);
	vty_out(vty, " l_dst_ip        :%x  %s", g_fem_conf.l_dst_ip[i]        , VTY_NEWLINE);
	vty_out(vty, " l_dst_prefixlen :%d  %s", g_fem_conf.l_dst_prefixlen[i] , VTY_NEWLINE);
	vty_out(vty, " sipto_status    :%d  %s", g_fem_conf.s_stat[i]    , VTY_NEWLINE);
	vty_out(vty, " s_gw_ip         :%x  %s", g_fem_conf.s_gw_ip[i]         , VTY_NEWLINE);
	vty_out(vty, " s_gw_prefixlen  :%d  %s", g_fem_conf.s_gw_prefixlen[i]  , VTY_NEWLINE);
	vty_out(vty, " s_dst_ip        :%x  %s", g_fem_conf.s_dst_ip[i]        , VTY_NEWLINE);
	vty_out(vty, " s_dst_prefixlen :%d  %s", g_fem_conf.s_dst_prefixlen[i] , VTY_NEWLINE);
	vty_out(vty, "============================================%s" , VTY_NEWLINE);
    }

    return CMD_SUCCESS;

}


struct mul_app_client_cb femto_app_cbs = {
    .switch_add_cb =  femto_add,
    .switch_del_cb = femto_del,
    .process_vendor_msg_cb = femto_vendor_msg
};  

void
femto_module_init(void *base_arg)
{
    struct event_base *base = base_arg;

    c_log_debug("%s", FN);

#if 0
    mul_register_app(NULL, FEMTO_APP_NAME, 
	    C_APP_ALL_SW, C_APP_ALL_EVENTS,
	    0, NULL, femto_event_notifier);
#else
    mul_register_app_cb(NULL, FEMTO_APP_NAME,
	    C_APP_ALL_SW, C_APP_ALL_EVENTS,
	    0, NULL, &femto_app_cbs);
#endif

    return;
}

    void
femto_module_vty_init(void *arg UNUSED)
{
    c_log_debug("%s:", FN);
    install_element(ENABLE_NODE, &femto_sipto_config_cmd);
    install_element(ENABLE_NODE, &femto_lipa_config_cmd);
    install_element(ENABLE_NODE, &show_femto_config_cmd);
}

module_init(femto_module_init);
module_vty_init(femto_module_vty_init);
